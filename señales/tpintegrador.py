#!/usr/bin/env python3
# -*- coding: utf-8 -*-
###############################################################################
#--------------------------------Imports--------------------------------------#
###############################################################################
from scipy.fftpack import fft, ifft, fftshift
from scipy import signal as sgn
from scipy import io
import numpy as np
import matplotlib.pyplot as plt

###############################################################################
#--------------------------Carga de los archivos------------------------------#
###############################################################################
file = io.loadmat('SAR_data_sarat.mat') #file['data']
fsarat = np.flipud(file['data'])
###############################################################################
#------------------------------Constantes-------------------------------------#
###############################################################################
v = 108 #m/s
fsamp = 50e6 #Hz
prf = 125 #Hz
fport = 1300e6 #Hz
lchirp = 10e-6 #seg
bandwidth = 38e6 #Hz
londa = 0.23 #m
r0 = 7545 #m
acimut = 0.107 #rad

###############################################################################
#-------------------------Definiciones iniciales------------------------------#
###############################################################################
#Función de fase del pulso chirp
def ffase(t, k1, k2, phi0):
    return k1*t**2+k2*t+phi0

#Función de pulso chirp
def chirp(t, k1, k2, phi0):
    return np.exp(2j*np.pi*ffase(t, k1, k2, phi0))

###############################################################################
#-----------------------------Ejercicio 1-------------------------------------#
###############################################################################
k1 = 1.9e12 #Hz^2
k2 = -19e6 #Hz
phi0 = 0 #rad

###############################################################################
#-----------------------------Ejercicio 2-------------------------------------#
###############################################################################
#Soporte de la función chirp (0s a 10e-6s)
t = np.arange(0, lchirp-1/fsamp, 1/fsamp)

#Función chirp muestreada a 50MHz
chirp_m = chirp(t, k1, k2, phi0)

#Función de fase muestreada a 50MHz
ffase_m = ffase(t, k1, k2, phi0)

#Grafico chirp - parte real
plt.plot(t, chirp_m.real, 'red', linewidth=0.5)
plt.ticklabel_format(axis='both', style='sci', scilimits=(-2,2))
plt.xlabel('Tiempo [s]')
plt.ylabel('Amplitud')
plt.title('Chirp - Parte Real')
plt.savefig('img/ej2(a).eps', format='eps')
plt.clf()

#Grafico chirp - parte imag
plt.plot(t, chirp_m.imag, 'blue', linewidth=0.5)
plt.ticklabel_format(axis='both', style='sci', scilimits=(-2,2))
plt.xlabel('Tiempo [s]')
plt.ylabel('Amplitud')
plt.title('Chirp - Parte Imaginaria')
plt.savefig('img/ej2(b).eps', format='eps')
plt.clf()

#Grafico funcion de fase
plt.plot(t, ffase_m, 'green', linewidth=0.5)
plt.ticklabel_format(axis='both', style='sci', scilimits=(-2,2))
plt.xlabel('Tiempo [s]')
plt.ylabel('Fase [rad]')
plt.title('Función de fase')
plt.savefig('img/ej2(c).eps', format='eps')
plt.clf()

#Cálculo de la fft del chirp
fft_chirp = fft(chirp_m)
fft_chirp = fftshift(fft_chirp)

#Eje de frecuencias para la fft del chirp (-19MHz a 19MHz)
w = np.linspace(-bandwidth//2, bandwidth//2, len(fft_chirp))

#Grafico de la fft del chirp
plt.plot(w, 20*np.log10(np.abs(fft_chirp)), 'purple', linewidth=0.5)
plt.ticklabel_format(axis='both', style='sci', scilimits=(-2,2))
plt.xlabel('Frecuencia [Hz]')
plt.ylabel('Amplitud [dB]')
plt.title('Espectro - Chirp')
plt.savefig('img/ej2(d).eps', format='eps')
plt.clf()

###############################################################################
#-----------------------------Ejercicio 3-------------------------------------#
###############################################################################
#Ventana Hanning
wdw_hann = sgn.tukey(50, alpha=1)

#Gráfico de la ventana Hanning
plt.plot(wdw_hann)
plt.title('Ventana Hanning')
plt.xlabel('Muestras')
plt.ylabel('Amplitud')
plt.savefig('img/ej3(hann).eps', format='eps')
plt.clf()

#Ventana Rectangular
wdw_ftop = sgn.tukey(50, alpha=0)

#Gráfico de la ventana Rectangular
plt.plot(wdw_ftop)
plt.title('Ventana Rectangular')
plt.xlabel('Muestras')
plt.ylabel('Amplitud')
plt.savefig('img/ej3(rect).eps', format='eps')
plt.clf()

#Espectrograma con ventana Hanning a fsamp 50MHz (Satisface Nyquist)
frec, tiempo, Sxx = sgn.spectrogram(chirp_m, fsamp, wdw_hann, noverlap=35, 
                                    return_onesided=False)
frec = fftshift(frec)
Sxx = fftshift(Sxx, axes=0)
plt.pcolormesh(tiempo, frec, Sxx)
plt.ticklabel_format(axis='both', style='sci', scilimits=(-2,2))
plt.title('Espectrograma - Hanning - 50MHz')
plt.ylabel('Frecuencia [Hz]')
plt.xlabel('Tiempo [s]')
plt.savefig('img/ej3(a).eps', format='eps')
plt.clf()

#Espectrograma con ventana Rectangular a fsamp 50MHz (Satisface Nyquist)
frec, tiempo, Sxx = sgn.spectrogram(chirp_m, fsamp, wdw_ftop, noverlap=35, 
                                    return_onesided=False)
frec = fftshift(frec)
Sxx = fftshift(Sxx, axes=0)
plt.pcolormesh(tiempo, frec, Sxx)
plt.ticklabel_format(axis='both', style='sci', scilimits=(-2,2))
plt.title('Espectrograma - Rectangular - 50MHz')
plt.ylabel('Frecuencia [Hz]')
plt.xlabel('Tiempo [s]')
plt.savefig('img/ej3(b).eps', format='eps')
plt.clf()

#Con fsamp2 = 34MHz no se detecta aliasing en el espectrograma
fsamp2 = fsamp*0.60 

#Vector de tiempos muestreado con fsamp2
t2 = np.arange(0, lchirp-1/fsamp2, 1/fsamp2)

#Chirp muestreado con fsamp2
chirp2_m = chirp(t2, k1, k2, phi0)

#Espectrograma con ventana Hanning a fsamp 34MHz (no Satisface Nyquist)
frec, tiempo, Sxx = sgn.spectrogram(chirp2_m, fsamp2, wdw_hann, noverlap=35, 
                                    return_onesided=False)
frec = fftshift(frec)
Sxx = fftshift(Sxx, axes=0)
plt.pcolormesh(tiempo, frec, Sxx)
plt.ticklabel_format(axis='both', style='sci', scilimits=(-2,2))
plt.title('Espectrograma - Hanning - 30MHz')
plt.ylabel('Frecuencia [Hz]')
plt.xlabel('Tiempo [s]')
plt.savefig('img/ej3(c).eps', format='eps')
plt.clf()

#Espectrograma con ventana Rectangular a fsamp 34MHz (no Satisface Nyquist)
frec, tiempo, Sxx = sgn.spectrogram(chirp2_m, fsamp2, wdw_ftop, noverlap=35, 
                                    return_onesided=False)
frec = fftshift(frec)
Sxx = fftshift(Sxx, axes=0)
plt.pcolormesh(tiempo, frec, Sxx)
plt.ticklabel_format(axis='both', style='sci', scilimits=(-2,2))
plt.title('Espectrograma - Rectangular - 30MHz')
plt.ylabel('Frecuencia [Hz]')
plt.xlabel('Tiempo [s]')
plt.savefig('img/ej3(d).eps', format='eps')
plt.clf()

###############################################################################
#-----------------------------Ejercicio 9-------------------------------------#
###############################################################################
#Función de Correlación cruzada entre dos señales por medio de FFT e IFFT
def correlacion(x1, x2):
    largo = len(x1)+len(x2)-1
    aux = ifft(np.conj(np.flip(fft(x1, largo))) * fft(x2, largo))
    return aux[len(x1):len(aux)]

#Autocorrelación de la señal chirp por medio de la función creada
corr_fft = correlacion(chirp_m, chirp_m)

#Autocorrelación de la señal chirp por medio de la función de Numpy
corr_np = np.correlate(chirp_m, chirp_m, 'full')

#Gráfico de la Autocorrelación hecha con la función creada
plt.plot(np.arange(0,len(corr_fft),1),np.abs(corr_fft), linewidth=0.5)
plt.ticklabel_format(axis='both', style='sci', scilimits=(-2,2))
plt.title('Autocorrelación - fft')
plt.ylabel('Amplitud')
plt.xlabel('Muestras')
plt.savefig('img/ej9(a).eps', format='eps')
plt.clf()

#Gráfico de la Autocorrelación hecha con la función de Numpy
plt.plot(np.arange(0,len(corr_np),1),20*np.log10(np.abs(corr_np)), linewidth=0.5)
plt.ticklabel_format(axis='both', style='sci', scilimits=(-2,2))
plt.title('Autocorrelación - np.correlate')
plt.ylabel('Amplitud')
plt.xlabel('Muestras')
plt.savefig('img/ej9(b).eps', format='eps')
plt.clf()

###############################################################################
#-----------------------------Ejercicio 10------------------------------------#
###############################################################################
#Función que correlacióna las filas de una matriz 'datos' con una 'señal'
def focalizar_datos(datos, señal):
    datos_focalizados = []
    aux = []
    for fila in datos:
        aux = correlacion(señal, fila)
        datos_focalizados.append(aux)
    return np.array(datos_focalizados)

#Focaliza los datos segun las filas (rango)
sarat_frg = focalizar_datos(fsarat, chirp_m) #sarat focalizado

#Gráfico del sarat sin focalizar en rango
plt.pcolormesh(10*np.log10(np.abs(fsarat)), cmap='terrain')
plt.title('Imagen Sarat - Sin focalizar en rango')
plt.ylabel('Acimut[Muestras]')
plt.xlabel('Rango[Muestras]')
plt.savefig('img/ej10(a).png', format='png', dpi=200)
plt.clf()

#Gráfico del sarat focalizado en rango
plt.pcolormesh(10*np.log10(np.abs(sarat_frg)), cmap='terrain')
plt.title('Imagen Sarat - Focalizado en rango')
plt.ylabel('Acimut[Muestras]')
plt.xlabel('Rango[Muestras]')
plt.savefig('img/ej10(b).png', format='png', dpi=200)
plt.clf()

###############################################################################
#-----------------------------Ejercicio 12------------------------------------#
###############################################################################
#Función de pulso chirp, pero con una función de fase corregida para el Acimut
def chirp_az(t, v, londa, r0, acimut, tita0):
    return np.exp(2j*np.pi*ffase_az(t, v, londa, r0, acimut, tita0))

#Función de fase de pulso chirp, pero corregida para el Acimut
def ffase_az(t, v, londa, r0, acimut, tita0):
    return -1*(((v*t)**2)/(londa*r0)) + tita0

#Ancho de banda temporal para el chirp (acimut)
tw_az = acimut*r0/v

#Vector de tiempos para el chirp sobre el acimut
t_az = np.arange(-tw_az/2, tw_az/2, 1/prf)

#Fase inicial del chirp (acimut)
tita0 = 0

#Chirp (acimut) muestreado con fsamp=prf
chirp_az_m = chirp_az(t_az, v, londa, r0, acimut, tita0)

#Espectrograma del chirp (acimut) con ventana Hanning a prf
frec, tiempo, Sxx = sgn.spectrogram(chirp_az_m, prf, wdw_hann, noverlap=35, 
                                    return_onesided=False)
frec = fftshift(frec)
Sxx = fftshift(Sxx, axes=0)
plt.pcolormesh(tiempo, frec, Sxx)
plt.ticklabel_format(axis='both', style='sci', scilimits=(-2,2))
plt.title('Espectrograma - Chirp(acimut)')
plt.ylabel('Frecuencia [Hz]')
plt.xlabel('Tiempo [s]')
plt.savefig('img/ej12(a).eps', format='eps')
plt.clf()

###############################################################################
#-----------------------------Ejercicio 13------------------------------------#
###############################################################################
#Matriz sarat focalizada tanto en rango como en acimut
sarat_focalizada = focalizar_datos(sarat_frg.transpose(), chirp_az_m)
sarat_focalizada = sarat_focalizada.transpose()

#Corto la imagen para no mostrar la muestras con datos del transitorio del 
#procesamiento
sarat_focalizada = sarat_focalizada[:,0:1750]

#Gráfico del sarat focalizado en rango y en acimut
plt.pcolormesh(20*np.log10(np.abs(sarat_focalizada)), cmap='terrain')
plt.title('Imagen Sarat - Focalizada en Rango y en Acimut')
plt.ylabel('Acimut[Muestras]')
plt.xlabel('Rango[Muestras]')
plt.savefig('img/ej13(a).png', format='png', dpi=200)
plt.clf()

###############################################################################
#-----------------------------Ejercicio 14------------------------------------#
###############################################################################
#Filtro promediador movil

#Submuestreado 4 veces en acimut
sarat_focalizada_s = []
for index, fila in enumerate(sarat_focalizada):
    if index % 4 == 0:
        sarat_focalizada_s.append(fila)
sarat_focalizada_s = np.array(sarat_focalizada_s)

#Gráfico del sarat focalizado en rango y en acimut, submuestreada
plt.pcolormesh(20*np.log10(np.abs(sarat_focalizada_s)), cmap='terrain')
plt.title('Imagen Sarat - Focalizada en Rango y en Acimut - Submuestreada')
plt.ylabel('Acimut[Muestras]')
plt.xlabel('Rango[Muestras]')
plt.savefig('img/ej14(b).png', format='png', dpi=200)
plt.clf()

###############################################################################
#-------------------------------Ejercicio 15----------------------------------#
###############################################################################
#Función de pulso chirp, pero con una función de fase corregida para el Acimut
def chirp_az_mejorado(t, v, londa, r0, acimut, tita0, index):
    return np.exp(2j*np.pi*ffase_az_mejorado(t, v, londa, r0, acimut, tita0, 
                                             index))

#Función de fase de pulso chirp, pero corregida para el Acimut
def ffase_az_mejorado(t, v, londa, r0, acimut, tita0, index):
    return -1*(((v*t)**2)/(londa*(r0+3*(index-650)))) + tita0

#Función que correlaciona cada fila con su chirp correspondiente,
def focalizador_acimut_mejorado(datos, señales):
    datos_focalizados = []
    aux = []
    for index, fila in enumerate(datos):
        aux = correlacion(señales[index], fila)
        datos_focalizados.append(aux)
    return np.array(datos_focalizados)

#Vector que contiene las señales chirp (acimut) corregidas segun la fila
señales_mejoradas = []
for index, _ in enumerate(sarat_frg):
    señales_mejoradas.append(chirp_az_mejorado(t_az, v, londa, r0, acimut, 
                                               tita0, index))

#Cálculo de la focalización mejorada
señales_mejoradas = np.array(señales_mejoradas)
sarat_focalizada = focalizador_acimut_mejorado(sarat_frg.transpose(), 
                                                        señales_mejoradas)
sarat_focalizada = sarat_focalizada.transpose()

#Submuestreado 4 veces en acimut
sarat_focalizada_s = []
for index, fila in enumerate(sarat_focalizada):
    if index % 4 == 0:
        sarat_focalizada_s.append(fila)
sarat_focalizada_s = np.array(sarat_focalizada_s)

#Corto la imagen para no mostrar la muestras con datos del transitorio del 
#procesamiento
sarat_focalizada_s = sarat_focalizada_s[:,0:1750]

#Gráfico del sarat focalizado en rango y en acimut
plt.pcolormesh(20*np.log10(np.abs(sarat_focalizada_s)), cmap='terrain')
plt.title('Imagen Sarat - Focalizada con corrección - Submuestreada')
plt.ylabel('Acimut[Muestras]')
plt.xlabel('Rango[Muestras]')
plt.savefig('img/ej15(a).png', format='png', dpi=200)
plt.clf()